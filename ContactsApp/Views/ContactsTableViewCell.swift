//
//  ContactsTableViewCell.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 24/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell
{
    //MARK: - IBOutlets reference
    @IBOutlet weak var contactImageView: CircularImageView!
    @IBOutlet weak var contactNamelabel: UILabel!
    @IBOutlet weak var contactFavouriteImageView: UIImageView!
    
    
    //MARK: - Lifecycle Methods
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool , animated: Bool)
    {
        super.setSelected(selected , animated: animated)
    }

    //MARK: - UI Methods
    func setupUI(WithContactDataModel contactData: ContactDataModel)
    {
        self.contactNamelabel.text = contactData.first_name + " " + contactData.last_name
        DataService.instance.getImage(WithImageKey: contactData.url , downloadImageURLString: contactData.profile_pic)
        { [weak self] (image , err) in
            if let img = image
            {
                DispatchQueue.main.async
                {
                    self?.contactImageView.image = img
                }
            }
            else
            {
                DispatchQueue.main.async
                {
                        self?.contactImageView.image = UIImage(named: "placeholder_photo")
                }
            }
        }
    }
    
}
