//
//  NetworkManager.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 23/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import UIKit

enum APIMethods
{
    case GetAllContactsList
    
    var apiMethodString: String
    {
        switch self
        {
            case .GetAllContactsList:
                return "contacts"
        }
    }
}

enum ResponseFormat
{
    case HTML
    case JSON
    
    var responseType: String
    {
        switch self
        {
            case .HTML:
                return "html"
            case .JSON:
                return "json"
        }
    }
}

class NetworkManager: NSObject
{
    //MARK: - Properties
    let baseURL                           = "http://gojek-contacts-app.herokuapp.com"
    let apiResponseFormat: ResponseFormat = .JSON
    var imageCache                        = NSCache<NSString, UIImage>()
    var alreadyAttemptedkeysSet           = Set<String>()
    
    //MARK: - Download Image Method
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(WithKeyString keyName: String , downloadImageURLString urlString: String , completion: @escaping (_ image: UIImage? , _ error: Error?) -> Void)
    {
        if self.alreadyAttemptedkeysSet.contains(keyName)
        {
            if let cachedImage = self.imageCache.object(forKey: keyName as NSString)
            {
                completion(cachedImage , nil)
            }
            else
            {
                completion(nil , nil)
            }
        }
        else
        {
            self.alreadyAttemptedkeysSet.insert(keyName)
            let optionalURL = URL(string: urlString)
            if let url = optionalURL
            {
                self.getData(from: url)
                { [weak self] data, response, error in
                    if let error = error
                    {
                        completion(nil, error)
                    }
                    else if let data = data, let image = UIImage(data: data)
                    {
                        self?.imageCache.setObject(image, forKey: keyName as NSString)
                        completion(image, nil)
                    }
                    else
                    {
                        completion(nil , ((error! as NSError).userInfo["error"] as! Error))
                    }
                }
            }
        }
    }
    
    
    //MARK: - Get Method Request
    func makeGetNetworkRequest(WithApiName apiName: APIMethods , AdditionalInfoToAppend additionalInfoString: String = "" , AndWithDataClosure dataClosure: ((Data?) -> ())?)
    {
        let url            = NSURL(string: self.getFinalURL(ForAPIName: apiName , AndResponseFormat: self.apiResponseFormat))
        let request        = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session        = URLSession.shared
        
        let mData = session.dataTask(with: request as URLRequest)
        { (data, response, error) -> Void in
            if let _ = response as? HTTPURLResponse
            {
                dataClosure?(data)
            }
            else
            {
                print("Error: \(String(describing: error))")
            }
        }
        mData.resume()
    }
}


//MARK: - Private Methods Extension
extension NetworkManager
{
    private func getFinalURL(ForAPIName apiName: APIMethods , AdditionalInfoToAppend additionInfoString: String = "" , AndResponseFormat responseFormat: ResponseFormat) -> String
    {
        let additionInfo: String
        if additionInfoString.count > 0
        {
            additionInfo = "/" + additionInfoString
        }
        else
        {
            additionInfo = ""
        }
        return (self.baseURL + "/" + apiName.apiMethodString + additionInfo + "." + responseFormat.responseType)
    }
}
