//
//  DataService.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 24/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import UIKit

class DataService: NSObject
{
    //MARK: - Properties
    private static let _instance = DataService()
    static var instance: DataService
    {
        return _instance
    }
    
    let networkManager = NetworkManager()
    
    
    //MARK: - API Methods
    func getAllContactsData(AndPerformCompletionHandler completionHandler: ((Data?) -> ())?)
    {
        self.networkManager.makeGetNetworkRequest(WithApiName: .GetAllContactsList)
        { (data) in
            completionHandler?(data)
        }
    }
    
    func getImage(WithImageKey keyName: String , downloadImageURLString urlString: String , AndCompletionHandler completionHadler: @escaping (_ image: UIImage? , _ error: Error?) -> Void)
    {
        self.networkManager.downloadImage(WithKeyString: keyName , downloadImageURLString: urlString , completion: completionHadler)
    }
}
