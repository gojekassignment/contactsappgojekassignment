//
//  CircularImageView.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 24/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import UIKit

@IBDesignable
class CircularImageView: UIImageView
{
    //MARK: - IBInspectables
    @IBInspectable var cBorderWidth: CGFloat = 5.0
    
    //MARK: - Lifecycle Methods
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.setupUI()
    }
    
    override func prepareForInterfaceBuilder()
    {
        super.prepareForInterfaceBuilder()
        self.setupUI()
    }
    
    //MARK: - UI Methods
    private func setupUI()
    {
        self.layer.cornerRadius = self.frame.self.width/2
        self.layer.borderWidth  = self.cBorderWidth
        self.layer.borderColor  = UIColor.white.cgColor
    }
}
