//
//  ContactDataModel.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 24/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import Foundation


struct ContactDataModel: Codable
{
    var id         : Int
    var first_name : String
    var last_name  : String
    var profile_pic: String
    var favorite   : Bool
    var url        : String
}
