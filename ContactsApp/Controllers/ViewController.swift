//
//  ViewController.swift
//  ContactsApp
//
//  Created by Sumit Makkar on 23/08/19.
//  Copyright © 2019 Sumit Makkar. All rights reserved.
//

import UIKit

class ViewController: UITableViewController
{
    //MARK: - Properties
    var allContactsDataSourceArray = [ContactDataModel]()
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        DataService.instance.getAllContactsData
        { [weak self] (data) in
            let str = String.init(data: data! , encoding: .utf8)
            print(str!)
            
            let decoder = JSONDecoder()
            if let allContacts = try? decoder.decode([ContactDataModel].self , from: data!)
            {
                self?.allContactsDataSourceArray = allContacts
                DispatchQueue.main.async
                {
                    self?.tableView.reloadData()
                }
            }
        }
    }
}

//MARK: - Data Source Methods
extension ViewController
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.allContactsDataSourceArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCellIdentifier" , for: indexPath) as? ContactsTableViewCell
        {
            cell.setupUI(WithContactDataModel: self.allContactsDataSourceArray[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}
